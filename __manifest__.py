# -*- coding: utf-8 -*-
{
    'name': "users_skills",

    'summary': """
        Users skills""",

    'description': """
        Modulo para guardar el nivel de experiencia de los usuarios
    """,

    'author': "Emmanuel Cruz",
    'website': "E",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/view_users_skill.xml',
        'views/view_skills.xml',
        'views/view_wizard_upload.xml',
        'views/view_res_partner.xml',
    ],
}
