# -*- coding: utf-8 -*-

from odoo import models, fields, exceptions, api


class UsersSkills(models.Model):
    '''
        Modelo para registrar por usuario las diferentes habilidades
    '''
    _name = 'users_skills.users_skills'
    _description = 'Habilidades de los usuarios'

    partner_id = fields.Many2one('res.partner', ondelete='cascade', string='Usuario', required=True)
    name = fields.Char(string='Nombre')
    skills_ids = fields.One2many('users_skills.skills', 'skills_id', string='Habilidades')
    years = fields.Integer(string='Años', default='0')
    company_id = fields.Many2one('res.company', string='Compania')

    _sql_constraints = [
        ('partner_id_unique', 'UNIQUE(partner_id)', 'Registro existente'),
    ]

    '''
        Metodo para traer nombre (verificar)
    '''
    # def _compute_name_get(self):
    #     skills_list = []
    #     for skill in self:
    #         name = skills.partner_id.name + ' ' + skills.skills.name + ' ' + str(skill.percent)
    #         skills_list.append((skills.id, name))
    #     return skill_list


class Skills(models.Model):
    _name = 'users_skills.skills'

    '''
        Modelo de habilidades para poder crear vista de One2Many en el modelo User skills
    '''

    toolwork = fields.Selection([('H', 'Herramienta'), ('F', 'Framework'), ('LP', 'Lenguaje de programacion')],
                                string='Herramienta/Framework/Lenguaje')
    name = fields.Char(required=True, string='Valor')
    skills_id = fields.Many2one('users_skills.users_skills', string='Habilidades', ondelete='set null')
    percent = fields.Float(string='Porcentaje (%)', default='0.0')

    _sql_constraints = [
        ('name_unique', 'UNIQUE(name)', 'Registro existente'),
    ]


class UploadFile(models.TransientModel):
    _name = 'users_skills.upload_file'

    upload_file = fields.Binary(string='Subir fichero', required=True)
    file_name = fields.Char(string='Nombre del fichero')

    def import_file(self):
        if self.file_name:
            if '.csv' not in self.file_name:
                raise exceptions.ValidationError('El archivo debe ser un CSV')
            file = self.read_file_from_binary(self.upload_file)
            lines = file.split('\n')
            for line in lines:
                elements = line.split(';')
                if len(elements) > 1:
                    self.env['users_skills.users_skills'].create({
                        'partner_id': elements[0],
                        'name': elements[1],
                        'skill': elements[2],
                        'years': 0,
                        'company_id': '',
                    })

    def read_file_from_binary(self, file):
        try:
            with io.BytesIO(base64.b64decode(file)) as f:
                f.seek(0)
                return f.read().decode('UTF-8')
        except Exception as e:
            print(str(e))
            raise e
