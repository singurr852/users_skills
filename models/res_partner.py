# -*- coding: utf-8 -*-

from odoo import models, fields, exceptions, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    number_skills = fields.Integer(compute='_compute_get_number_skills')

    def _compute_get_number_skills(self):
        self.ensure_one()
        for skill in self:
            skill.number_skills = self.env['users_skills.users_skills'].search_count([('partner_id', '=', self.id)])
